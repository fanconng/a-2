import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.neural_network.multilayer_perceptron import MLPRegressor
import matplotlib.pyplot as plt

from nn_regression_plot import plot_mse_vs_neurons, plot_mse_vs_iterations, plot_learned_function, \
    plot_mse_vs_alpha, plot_bars_early_stopping_mse_comparison

"""
Computational Intelligence TU - Graz
Assignment 2: Neural networks
Part 1: Regression with neural networks

This file contains functions to train and test the neural networks corresponding the the questions in the assignment,
as mentioned in comments in the functions.
Fill in all the sections containing TODO!
"""

__author__ = 'bellec,subramoney'


def calculate_mse(nn, x, y):
    """
    Calculate the mean squared error on the training and test data given the NN model used.
    :param nn: An instance of MLPRegressor or MLPClassifier that has already been trained using fit
    :param x: The data
    :param y: The targets
    :return: Training MSE, Testing MSE
    """
    ## TODO
    mse = 0

    y_pred = nn.predict(x)
    mse = mean_squared_error(y, y_pred)

    return mse


def ex_1_1_a(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 1.1 a)
    Remember to set alpha to 0 when initializing the model
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    ## TODO
    list_ = [2, 8, 40]

    for i in list_:
        regression = MLPRegressor(hidden_layer_sizes=(i,),
                                   activation='logistic',
                                   solver='lbfgs',
                                   max_iter=200,
                                   random_state=1,
                                   alpha=0)

        regression.fit(x_train, y_train)

        y_pred_test = regression.predict(x_test)
        plot_learned_function(i, x_train, y_train, None, x_test, y_test, y_pred_test)


def ex_1_1_b(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 1.1 b)
    Remember to set alpha to 0 when initializing the model
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """
    ## TODO
    size = (10 ,)
    mse_test = np.zeros(size)
    mse_train = np.zeros(size)

    mean_test = 0
    mean_train = 0

    for i in range(10):
        regression = MLPRegressor(hidden_layer_sizes=(8,),
                                  activation='logistic',
                                  solver='lbfgs',
                                  max_iter=200,
                                  random_state=i,
                                  alpha=0,
                                  warm_start=True)
        regression.fit(x_train, y_train)
        mse_test[i] = calculate_mse(regression, x_test, y_test)
        mse_train[i] = calculate_mse(regression, x_train, y_train)
    
def ex_1_1_c(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 1.1 c)
    Remember to set alpha to 0 when initializing the model
    Use max_iter = 10000 and tol=1e-8
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    ## TODO

    list_ = [1, 2, 3, 4, 6, 8, 12, 20, 40]

    size = (9, 10)
    matrix_train = np.zeros(size)
    matrix_test = np.zeros(size)

    for iterator in range(9):
        for rand in range(10):
            regression = MLPRegressor(hidden_layer_sizes=(list_[iterator],),
                                      activation='logistic',
                                      solver='lbfgs',
                                      max_iter=10000,
                                      tol=1e-8,
                                      random_state=rand,
                                      alpha=0)
            regression.fit(x_train, y_train)
            mse = calculate_mse(regression, x_train, y_train)
            matrix_train[iterator][rand] = mse
            mse = calculate_mse(regression, x_test, y_test)
            matrix_test[iterator][rand] = mse

    plot_mse_vs_neurons(matrix_train, matrix_test, list_)

    regression.fit(x_train, y_train)
    y_pred_test = regression.predict(x_test)
    plot_learned_function(40, x_train, y_train, None , x_test, y_test, y_pred_test)

def ex_1_1_d(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 1.1 b)
    Remember to set alpha to 0 when initializing the model
    Use n_iterations = 10000 and tol=1e-8
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """
    ## TODO

    list_ = [2, 8, 40]

    size = (3, 10000)
    matrix_train = np.zeros(size)
    matrix_test = np.zeros(size)


    for i in range(3):
        regression = MLPRegressor(hidden_layer_sizes=(list_[i],),
                                  activation='logistic',
                                  solver='lbfgs',
                                  max_iter=1,
                                  warm_start=True,
                                  random_state=0,
                                  alpha=0)
        for iteraton in range(10000):
            regression.fit(x_train, y_train)
            mse = calculate_mse(regression, x_train, y_train)
            matrix_train[i][iteraton] = mse
            regression.fit(x_test, y_test)
            mse = calculate_mse(regression, x_test, y_test)
            matrix_test[i][iteraton] = mse

    #plot_mse_vs_iterations(train_mses, test_mses, n_iterations, hidden_neuron_list)
    plot_mse_vs_iterations(matrix_train, matrix_test, 10000, len(list_))
    
    for i in range(3):
        regression = MLPRegressor(hidden_layer_sizes=(list_[i],),
                                  activation='logistic',
                                  solver='sgd',
                                  max_iter=1,
                                  warm_start=True,
                                  random_state=0,
                                  alpha=0)
        for iteraton in range(10000):
            regression.fit(x_train, y_train)
            mse = calculate_mse(regression, x_train, y_train)
            matrix_train[i][iteraton] = mse
            regression.fit(x_test, y_test)
            mse = calculate_mse(regression, x_test, y_test)
            matrix_test[i][iteraton] = mse



def ex_1_2_a(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 1.2 a)
    Remember to set alpha to 0 when initializing the model
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """
    ## TODO


    list_ = [pow(10, -8), pow(10, -7), pow(10, -6), pow(10, -5), pow(10, -4), pow(10, -3), pow(10, -2), pow(10, -1),
             1, 10, 100]

    size = (len(list_), 10)
    matrix_train = np.zeros(size)
    matrix_test = np.zeros(size)

    for x in range(len(list_)):
        for y in range(10):
            regression = MLPRegressor(hidden_layer_sizes=(8,),
                                      activation='logistic',
                                      solver='lbfgs',
                                      max_iter=200,
                                      random_state=np.random.randint(100),
                                      alpha=list_[x])
            regression.fit(x_train, y_train)
            mse = calculate_mse(regression, x_train, y_train)
            matrix_train[x][y] = mse
            regression.fit(x_test, y_test)
            mse = calculate_mse(regression, x_test, y_test)
            matrix_test[x][y] = mse

    plot_mse_vs_alpha(matrix_train, matrix_test, list_)



def ex_1_2_b(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 1.2 b)
    Remember to set alpha and momentum to 0 when initializing the model
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """
    ## TODO
    pass


def ex_1_2_c(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 1.2 c)
    :param x_train:
    :param x_test:
    :param y_train:
    :param y_test:
    :return:
    """
    ## TODO
    pass
